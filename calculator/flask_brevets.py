"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


#client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
client = MongoClient('172.18.0.2', 27017)

db = client.tododb

numRows = 20
#rows = [dict() for x in range(numRows)]
rows = [{'row':r,'open':False,'close':False} for r in range(numRows)]

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    _items = db.tododb.find()
    items = [item for item in _items]
    return flask.render_template('calc.html',items=items)

@app.route("/update_db", methods=['POST'])
def new():
    db.tododb.delete_many({})
    global rows
    for x in range(numRows):
        item_doc = {
            'row': rows[x]['row'],
            'open_time': rows[x]['open'],
            'close_time': rows[x]['close']
        }
        db.tododb.insert_one(item_doc)
    rows = [{'row':r,'open':False,'close':False} for r in range(numRows)]

    return redirect(url_for('index'))
    

@app.route("/display", methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    if len(items) < 1:
        return flask.render_template('calc.html',items=items)
    return flask.render_template('display.html',items=items)

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    row = request.args.get('row',type=int)
    km = request.args.get('km', 999, type=float)

    distance = request.args.get('distance',type=int)

    begin_date = request.args.get('begin_date') + " " + request.args.get('begin_time')
  
    begin_date_formatted = arrow.get(begin_date).isoformat()

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, distance, begin_date_formatted)
    close_time = acp_times.close_time(km, distance, begin_date_formatted)
    rows[row]['row'] = row;
    rows[row]['open'] = open_time;
    rows[row]['close'] = close_time;

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result) 

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
