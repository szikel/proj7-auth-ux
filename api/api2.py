#!/usr/bin/env python
# Laptop Service

import os
import pymongo
import flask
import arrow  # Replacement for datetime, based on moment.js
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
from pymongo import MongoClient

# Instantiate the app
app = Flask(__name__)
api = Api(app)

#client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
client = MongoClient('172.18.0.2', 27017)
db = client.tododb

newEntry = {
            'row': 0,
            'open_time': 0,
            'close_time': 0
            }

controlList = [];
class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }

# Create routes
# Another way, without decorators
#api.add_resource(Laptop, '/')

class Control(Resource):
    def get(self):
        #return newEntry
        return controlList

api.add_resource(Control, '/listAll')
numRows = 20
#rows = [dict() for x in range(numRows)]
rows = [{'row':r,'open':False,'close':False} for r in range(numRows)]

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    _items = db.tododb.find()
    items = [item for item in _items]
    return flask.render_template('calc.html',items=items)

@app.route("/update_db", methods=['POST'])
def new():
    db.tododb.delete_many({})
    
    global controlList
    global rows
    controlList.clear()
    for x in range(numRows):
        if rows[x]['open'] is not False:
            item_doc = {
                'row': rows[x]['row'],
                'open_time': rows[x]['open'],
                'close_time': rows[x]['close']
            }
        
            newEntry['row'] = item_doc['row']
            newEntry['open_time'] = item_doc['open_time']
            newEntry['close_time'] = item_doc['close_time']
            controlList.append(newEntry)
            #controlList.append(item_doc)

            db.tododb.insert_one(item_doc)
    rows = [{'row':r,'open':False,'close':False} for r in range(numRows)]

    return redirect(url_for('index'))


@app.route("/display", methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    if len(items) < 1:
        return flask.render_template('calc.html',items=items)
    return flask.render_template('display.html',items=items)

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    row = request.args.get('row',type=int)
    km = request.args.get('km', 999, type=float)

    distance = request.args.get('distance',type=int)

    begin_date = request.args.get('begin_date') + " " + request.args.get('begin_time')

    begin_date_formatted = arrow.get(begin_date).isoformat()

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    #open_time = acp_times.open_time(km, distance, begin_date_formatted)
    #close_time = acp_times.close_time(km, distance, begin_date_formatted)
    open_time = 4;
    close_time = 5;

    rows[row]['row'] = row;
    rows[row]['open'] = open_time;
    rows[row]['close'] = close_time;

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

#############

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
